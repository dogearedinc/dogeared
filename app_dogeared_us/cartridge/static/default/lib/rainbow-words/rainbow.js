  var Rainbow = (function (window, document, undefined) {

    function init(options) {
      if(typeof options !== 'object') {
        throw new TypeError('Options should be an object');
      }

      var selector = document.querySelector(options.element),
        letters = Array.prototype.slice.call(selector.textContent.trim()),
        colors = typeof options.colors === 'string' ? Array.prototype.slice.call(options.colors, ',') : options.colors,
        customWrapper = options.wrapper;

      var len = letters.length,
        frag = document.createDocumentFragment();
      for (var i = 0; i < len; i++) {
        var wrapper = document.createElement(customWrapper);
        wrapper.style.color = colors[i];
        wrapper.textContent = letters[i];
        frag.appendChild(wrapper);
      }

      selector.innerHTML = '';
      selector.appendChild(frag);

    }

    return {
      paint: init
    };
  })(window, document);

