'use strict';

var ajax = require('./ajax'), 
	util = require('./util');
/**
 * @function
 * @description Load details of a given loyalty card
 * @param {String}
 *            id The ID of the loyalty card
 * @param {String}
 *            pin of the loyalty card
 * @param {Function}
 *            callback A function to called
 */
exports.checkBalance = function(id, pin, callback) {
	// load gift certificate details
	var url = util.appendParamToURL(Urls.loyaltycardCheckBalance, 'loyaltyCardID', id);
	url = util.appendParamToURL(url, "loyaltyCardPin", pin);

	ajax.getJson({
		url : url,
		callback : callback
	});
};

/**
 * @function
 * @description Saves the loyalty card usage choice.
 * @param {String} loyaltyCardId
 * @param {String} loyaltyCardPin
 * @param {Boolean} redeemBalance
 * @param {Function} callback
 */
exports.redeemLoyaltyCard = function(loyaltyCardId, loyaltyCardPin, redeemBalance, callback) {
	// attempt to redeem loyalty Card
	var url = util.appendParamToURL(Urls.redeemLoyaltycard, "LoyaltyCardID", loyaltyCardId);
	var url = util.appendParamToURL(url, "LoyaltyPin", loyaltyCardPin);
	var url = util.appendParamToURL(url, "isloyaltyCard", true);
	var url = util.appendParamToURL(url, "loyaltyRedemption", redeemBalance);

	var result = ajax.getJson({
		url : url,
		callback : callback});
};

/**
 * @function
 * @description Converts a balance hashmap to a human-readable description of the balances
 * @param {Object} balances		Object of currency/type => amount pairs
 * @returns {String}
 */
exports.toText = function(balances) {
	var text = "";
	
	// Return a different message if the card has no balances
	if (!balances || jQuery.isEmptyObject(balances)) {
		text = Resources.LOYALTY_CARD_NO_BALANCE;
	} else {
		// Display loyalty balances
		var text = Resources.LOYALTY_CARD_BALANCE;
		var message_parts = [];
		jQuery.map(balances, function(key, value) {
			if (key != "ID") {
				message_parts.push(value + " " + key);
			}
		});
		text += message_parts.join(", ") ;
	}

	return text;
};

