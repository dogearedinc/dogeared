(function($,sr){
 
  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  /* var debounce = function (func, threshold, execAsap) {
      var timeout;
 
      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null; 
          };
 
          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);
 
          timeout = setTimeout(delayed, threshold || 500); 
      };
   } */
   
   function debounce(fn, delay) {
	  var timer = null;
	  return function () {
		var context = this, args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
		  fn.apply(context, args);
		}, delay || 300);
	  };
	}
	// smartresize 
	jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };
 
}(jQuery,'smartresize'));


/*
 * All java script logic for the mobile layout.
 *
 * The code relies on the jQuery JS library to
 * be also loaded.
 *
 * The logic extends the JS namespace app.*
 */
(function(app, $, undefined) {
	
	app.responsive = {
	
		mobileLayoutWidth : 480,
		
		init : function () {

			$cache = {
                            wrapper: $('#wrapper'),
                            navigation: $('#navigation'),
                            homepageSlider: $('#homepage-slider'),
                            zoom: $('#pdpMain').find('a.main-image'),
                            minicart: $('#mini-cart'),
                            header: $('#header'),
                            headerHeight: $('#header').outerHeight(),
                            bagHeight: $('.mini-cart-content').outerHeight(),
                            windowHeight: $(window).outerHeight(),
                            productPrice: $('.product-add-to-cart fieldset .clearfix .product-price'),
                            emailSubscribeBtn : $('#email-alert-signup input[type="submit"]'),
                            productInventory: $('.product-add-to-cart fieldset .clearfix .inventory')
                        };
			
			// check onload to see if mobile enabled
			if( $cache.wrapper.width() <= this.mobileLayoutWidth  || jQuery('#wrapper').width() < 750) {
				app.responsive.enableMobileNav();
				$cache.emailSubscribeBtn.val('');
			}
			
		},
		
		//checks to see if the mini bag is taller then the window
		variableMiniBagHeight : function(){
			var bagOpenHeight = $cache.headerHeight + $cache.bagHeight;
			
			if($cache.windowHeight < bagOpenHeight){
				$cache.header.addClass('positionRelative');
			}
		}, 
		
		// build vertical, collapsable menu
		enableMobileNav : function(){
			
			// create the dropdown arrow for the menu element
			$cache.navigation.find('.menu-category div.level-2')
				.each(function(){
					if( $(this).length > 0 ) {
						if( $(this).siblings('.nav-close').length > 0 ){
						} else{
							$('<span class="nav-close"></span>').insertAfter($(this).siblings('a'));
						}
					}
				});
				
				
			// expand the nav level-2 when click on the span
			$cache.navigation.find('.nav-close')
				.on('click', function(){
					
					
					if ( $(this).parent('li').hasClass('nav-expanded') ){
						$(this).siblings('div.level-2')
							.hide()
							.parent('li').removeClass('nav-expanded');
					} else {
						$(this).siblings('div.level-2')
							.show()
							.parent('li').addClass('nav-expanded');
					} 
					
					
				});
			
			// move navigation for homepage below the slider
			$('.pt_storefront #navigation').insertAfter('.pt_storefront .home-main');
			
			// adjust for blackbar promotion
		
		},
		
		// revert to standard horizontal menu
		disableMobileNav : function(){
			$cache.navigation.find('.menu-category').show();
			$cache.navigation.find('.level-2').removeAttr('style');
			$cache.navigation.find('.level-1 span').remove();
			
			// move navigation for homepage above the slider
			$('.pt_storefront #navigation').insertAfter('#stickyHeader #header');
		
		},
		
		// pull the slideshow into a variable to re-use
		rebuildHomepageSlides: function() {
			if($cache.homepageSlider.length > 0){
				var homeCarousel = 	$cache.homepageSlider.data('jcarousel');
				homeCarousel.stopAuto();
				homeCarousel.scroll(1);
				$cache.homepageSlider.find('.slide').width( $cache.wrapper.width());
				$cache.homepageSlider.find('#homepage-slides').css( {'left':'0','width': ($cache.wrapper.width() * $cache.homepageSlider.find('#homepage-slides li').size()) });
				homeCarousel.reload();
			}
		},
		
		toggleGridWideTileView : function(){
			
			/*	toggle grid/wide tile	*/
			if(jQuery('.toggle-grid').length == 0 && (jQuery('.pt_order').length == 0) && (jQuery('.pt_content-search-result').length == 0))
			{
				jQuery('.results-hits').prepend('<a class="toggle-grid" href="'+location.href+'">+</a>');
				jQuery('.toggle-grid').click(function(){
					jQuery('.search-result-content').toggleClass('wide-tiles');
					return false;
				});
			}	
		},

		enableMobileSearch : function(){
			
			if( jQuery('#wrapper').width() <= app.responsive.mobileLayoutWidth || jQuery('#wrapper').width() < 750 ) {
				var searchBlock = $('.search-toggle-block');
				$('#q').remove().insertBefore('.search-toggle-block input'); // move the input below the search-toggle-btn
				
				searchBlock.removeClass('mobile');
				$('#q').attr('placeholder', 'Search');
				$('#q').addClass('search-input-expanded');// and a class is added on the search input ( to know that the search-toggle-block has been expanded)
				searchBlock.find('input[type="submit"]').addClass('search-input-expanded');	
			}
		},

		disableMobileSearch : function(){
			if( jQuery('#wrapper').width() > app.responsive.mobileLayoutWidth   ) {
				$('#q').insertBefore('.search-toggle-block').show();  // move the input back and show it
			}
		},
		
		miniCartInitResponsive : function(){
			var cartOpen = false;
			$cache.minicart.find('.mini-cart-link').on('click',function(e){
				e.preventDefault();
			});
			$cache.minicart.on("click", ".mini-cart-total", function(){
				if ($cache.minicart.find(".mini-cart-content").is(":visible")) {
					app.minicart.close();
					if($cache.minicart.hasClass('mobile-mini-cart-open')){
						$('#mini-cart').removeClass('mobile-mini-cart-open');
					}
					cartOpen = false;
					//$(document).off('click');
				}
				else {
					app.minicart.show();
//					$('.mini-cart-content').width($("body").innerWidth());
					$('#main').addClass('marginTopMinus5px');
					setTimeout(app.responsive.variableMiniBagHeight, 100);
					cartOpen = true;	
//					setTimeout(function(){
//						$(document).on('click',function(e) {
//						    var target = e.target;
//						    //console.log(target);
//						    //console.log(cartOpen);
//						    
//								if (!$(target).is('.mini-cart-content') && !$(target).is('.mini-cart-total') && !$(target).is('#mini-cart')) {
//							    	if ($cache.minicart.find(".mini-cart-content").is(":visible")) {
//							    		if (cartOpen) {
//							    			app.minicart.close();
//							    			//$(document).off('click');
//							    		}
//							    	}
//							    }
//						    
//						});			
//					}, 200);
				}
			});
			
		},
		
		miniCartDestroyResponsive : function(){
			$cache.minicart.find('.mini-cart-link').off('click');
			app.minicart.init();
		},
		
		promotionActiveModifications : function(){
			//checking if the promo block is visible or not
			if ($('.promo-container-mobile .promotion:visible').length == 1){
				//adding classes for portrait view, so the promo block will be in proper position
				if (jQuery('#wrapper').width() <= app.responsive.mobileLayoutWidth) {
					$cache.productPrice.addClass('priceWidthModificationsPortrait');
					$cache.productInventory.addClass('priceWidthModificationsPortrait');
					//this will handle when rotating screen from landscape to portrait view
					if ($cache.productPrice.hasClass('priceWidthModificationsPortrait') && $cache.productInventory.hasClass('priceWidthModificationsPortrait')) {
						$cache.productPrice.removeClass('priceWidthModificationsLandscape');
						$cache.productPrice.removeClass('priceWidthModificationsLandscape');
					}
				//adding classes for landscape view, so the promo block will be in proper position
				} else if (jQuery('#wrapper').width() > app.responsive.mobileLayoutWidth && jQuery('#wrapper').width() < 750) {
					$cache.productPrice.addClass('priceWidthModificationsLandscape');
					$cache.productInventory.addClass('priceWidthModificationsLandscape');
					//this will handle when rotating from portrait to landscape view
					if ($cache.productPrice.hasClass('priceWidthModificationsLandscape') && $cache.productInventory.hasClass('priceWidthModificationsLandscape')) {
						$cache.productPrice.removeClass('priceWidthModificationsPortrait');
						$cache.productPrice.removeClass('priceWidthModificationsPortrait');
					}
				}
			}
		},
		
		fixShippingSpace : function () {
			if($('shipping-details-lightbox .form-row h2').text() == "UPS"){
				console.log($(this));
			}
		}
	}
	
	
	$(document).ready(function(){
		
		app.responsive.init();
		app.responsive.enableMobileSearch();
		app.responsive.miniCartInitResponsive();
		app.responsive.promotionActiveModifications();
		app.responsive.fixShippingSpace();
		
		$('.search-toggle-btn').on('click', function(){ // when the search-toggle-btn is clicked
			$(this).toggleClass('mobileSearchOpen');
			$('.search-toggle-block').toggleClass("open");
			if($('#navigation').hasClass('menuOpen')){
				$('#navigation').removeClass('menuOpen');
			};						
		});
		
		// set up listener so we can update DOM if page grows/shrinks, only on bigger platforms
		if(screen.width > 750){
			$(window).smartresize(function(){
				
				if( jQuery('#wrapper').width() <= app.responsive.mobileLayoutWidth || jQuery('#wrapper').width() < 750 ) {
					app.responsive.enableMobileNav();
					app.responsive.rebuildHomepageSlides();
					app.responsive.miniCartInitResponsive();
					app.responsive.enableMobileSearch();
					app.responsive.variableMiniBagHeight();
					app.responsive.promotionActiveModifications();
					app.responsive.fixShippingSpace();
				}
				else {
					app.responsive.disableMobileNav();
					app.responsive.rebuildHomepageSlides();
					app.responsive.miniCartDestroyResponsive();
					app.responsive.disableMobileSearch();
				}
			});
		}
		
		
		// grid - move the refinements above the main section
		if( jQuery('#wrapper').width() <= app.responsive.mobileLayoutWidth || jQuery('#wrapper').width() < 750) {
			$('.pt_product-search-result #secondary').insertBefore('.pt_product-search-result #primary');
			$(window).hashchange(function () {
				$('.pt_product-search-result #secondary').insertBefore('.pt_product-search-result #primary');
			}).trigger('hashchange');
		
		} else{  // grid - move the refinements back below the main section
			$('.pt_product-search-result #secondary').insertBefore('.pt_product-search-result #primary');
		}
		
		// create account page modifications
		$('.pt_account label[for="dwfrm_profile_login_password"]').siblings('.form-caption').insertAfter('.pt_account label[for="dwfrm_profile_login_password"]');
		
		$('.refinement .nav-close').on('click', function(){
			$(this).parent('h3').toggleClass('nav-expanded');
		});
		
		
		$('.above-cart-footer .cart-coupon-code-content input').attr('placeholder', 'enter coupon code');
		
		if($('shipping-details-lightbox .form-row h2').text() == "UPS"){
			console.log($(this));
		}
	});
	
	
		
}(window.app = window.app || {}, jQuery));
