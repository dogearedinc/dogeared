// JavaScript Document

var anim_chain_pid = 0;
var anim_charm_tag1 = '';
var anim_charm_tag2 = '';

var anim_charm_pid1_1 = 0;
var anim_charm_pid2_1 = 0;
var anim_charm_pid2_2 = 0;

var droppable_offset;

function demo_anim2() {

	get_pids();

	console.log(anim_chain_pid);
	
	console.log(anim_charm_tag1);
	console.log(anim_charm_tag2);
	
	console.log(anim_charm_pid1_1);
	
	console.log(anim_charm_pid2_1);
	console.log(anim_charm_pid2_2);

	$("#inner_droppable").children().remove();
	remove_connector();

	step1_clicked(false);

	droppable_offset = $('#droppable').offset();

	init_scroll_window();

	$.play();
}

function get_pids() {

	//----------------
	// choose a chain
	//----------------
	var max = 4; // choose from the first four chains
	var num = Math.floor(Math.random() * max);
	
	var chains = $("#tabs-connector .connector");
	var chain = chains[num];
	
	// chain's product id
	anim_chain_pid = $(chain).attr("pid");
	
	//-----------------------------------------
	// choose two tags from left nav categories
	//-----------------------------------------
	var max = 18; // choose from the first eighteen categories
	var num1 = Math.floor(Math.random() * max);
	var num2 = Math.floor(Math.random() * max);
	
	var categories = $("#leftnav_charms .category");
	var category1 = categories[num1];
	var category2 = categories[num2];
	
	anim_charm_tag1 =  $(category1).attr("data-tag");
	anim_charm_tag2 =  $(category2).attr("data-tag");

	//------------------------------
	// choose 1 charm from first tag
	//------------------------------
	var charms = $('#tabs-'+anim_charm_tag1).find(".charm");

	var max = 18; // choose from the first 18 charms
	if (max > charms.length) { max = charms.length-1; }
	var num = Math.floor(Math.random() * max);

	var charm = charms[num];
	anim_charm_pid1_1 = $(charm).attr("pid");

	//--------------------------------
	// choose 2 charms from second tag
	//--------------------------------
	
	var charms = $('#tabs-'+anim_charm_tag2).find(".charm");

	var max = 18; // choose from the first 18 charms
	if (max > charms.length) { max = charms.length-1; }
	var num1 = Math.floor(Math.random() * max);
	var num2 = Math.floor(Math.random() * max);

	var charm1 = charms[num1];
	var charm2 = charms[num2];
	anim_charm_pid2_1 = $(charm1).attr("pid");
	anim_charm_pid2_2 = $(charm2).attr("pid");
	
}

function init_scroll_window() {
	
	$( window ).tween({
		scroll:{
			stop: (droppable_offset.top-40),
			time: 0,
			duration: 1,
			effect:'easeInOut',
			onStop: function( element ){
				drag_chain();
			}
		}
	});
}

function drag_chain() {
	
	var connector = $('#connector-'+anim_chain_pid).clone();
	var offset = $('#connector-'+anim_chain_pid).offset();
	connector.appendTo(document.body);
	connector.css("position","absolute");
		
	connector.tween({
		left:{ start: offset.left, stop: (droppable_offset.left+10), time: 0, units: 'px', duration: 1, effect:'easeInOut' },
		top:{ start: offset.top, stop: (droppable_offset.top+10), time: 0, units: 'px', duration: 1, effect:'easeInOut' },
		onStop: function( element ){
			connector.remove();
			create_add_to_droppable(null, "connector", anim_chain_pid);
			step2_clicked(false);
	
			drag_charm1_1();
	   }
	});
}

function drag_charm1_1() {

	show_tab('tabs-'+anim_charm_tag1);

	var charm1_1 = $('#tabs-'+anim_charm_tag1).find('#charm-'+anim_charm_pid1_1).clone();
	var offset = $('#tabs-'+anim_charm_tag1).find('#charm-'+anim_charm_pid1_1).offset();
	
	charm1_1.appendTo(document.body);
	charm1_1.css("position","absolute").css("display","block");
	charm1_1.find( "img" ).each(function() {
		var img_src = $( this ).attr('img_src') ;
		$( this ).attr('src', img_src) 
	});
	
	charm1_1.tween({
		left:{ start: offset.left, stop: (droppable_offset.left+340), time: 1, units: 'px', duration: 2, effect:'circOut' },
		top:{ start: offset.top, stop: (droppable_offset.top+20), time: 1, units: 'px', duration: 2, effect:'easeInOut' },
		onStop: function( element ){
			charm1_1.remove();
			create_add_to_droppable(null, "charm", anim_charm_pid1_1);
			
			drag_charm2_1();
		}
	});
}

function drag_charm2_1() {

	show_tab('tabs-'+anim_charm_tag2);

	var charm2_1 = $('#tabs-'+anim_charm_tag2).find('#charm-'+anim_charm_pid2_1).clone();
	var offset = $('#tabs-'+anim_charm_tag2).find('#charm-'+anim_charm_pid2_1).offset();
	
	charm2_1.appendTo(document.body);
	charm2_1.css("position","absolute").css("display","block");
	charm2_1.find( "img" ).each(function() {
		var img_src = $( this ).attr('img_src') ;
		$( this ).attr('src', img_src) 
	});
	
	charm2_1.tween({
		left:{ start: offset.left, stop: (droppable_offset.left+340+30), time: 1, units: 'px', duration: 2, effect:'circOut' },
		top:{ start: offset.top, stop: (droppable_offset.top+20), time: 1, units: 'px', duration: 2, effect:'easeInOut' },
		onStop: function( element ){
			charm2_1.remove();
			create_add_to_droppable(null, "charm", anim_charm_pid2_1);
			
			drag_charm2_2();
		}
	});
}

function drag_charm2_2() {

	var charm2_2 = $('#tabs-'+anim_charm_tag2).find('#charm-'+anim_charm_pid2_2).clone();
	var offset = $('#tabs-'+anim_charm_tag2).find('#charm-'+anim_charm_pid2_2).offset();
	
	charm2_2.appendTo(document.body);
	charm2_2.css("position","absolute").css("display","block");
	charm2_2.find( "img" ).each(function() {
		var img_src = $( this ).attr('img_src') ;
		$( this ).attr('src', img_src) 
	});
	
	charm2_2.tween({
		left:{ start: offset.left, stop: (droppable_offset.left+340+30+30), time: 1, units: 'px', duration: 2, effect:'circOut' },
		top:{ start: offset.top, stop: (droppable_offset.top+20), time: 1, units: 'px', duration: 2, effect:'easeInOut' },
		onStop: function( element ){
			charm2_2.remove();
			create_add_to_droppable(null, "charm", anim_charm_pid2_2);
		}
	});
}
