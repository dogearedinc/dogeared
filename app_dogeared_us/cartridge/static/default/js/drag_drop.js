// JavaScript Document

(function ( $ ) {
	$.fn.charm_popup = function() {

		this.hover(
			function () {
				var parent = $(this).parent();
				parent.find(".charm_hover_popup").css("display","none");

				var charm_hover_popup = $(this).find(".charm_hover_popup");

				var position = $(this).position();
				var position_top = parseInt(position.top, 10);
				var position_left = parseInt(position.left, 10);
				
				var width = parseInt($(this).css("width"), 10);

				var popup_height = parseInt(charm_hover_popup.css("height"), 10);
				var popup_width = parseInt(charm_hover_popup.css("width"), 10);
				
				charm_hover_popup.css("top", -(popup_height)+"px").css("left", parseInt( ((width/2) - (popup_width/2)), 10)+"px");
				charm_hover_popup.css("display","block");
			},
			function () {
				$(this).find(".charm_hover_popup").css("display","none");
			}
		);

		return this;
	};

	$.fn.connector_popup = function() {

		this.hover(
			function () {
				var parent = $(this).parent();
				$(this).find(".charm_hover_popup").fadeIn(300, function() { parent.find(".charm_hover_popup").css("display","none"); $(this).css("display","block"); } );
			},
			function () {
				$(this).find(".charm_hover_popup").css("display","none");
			}
		);

		return this;
	};

}( jQuery ));

var charms_counter = 0;
var MAX_NUM_CHARMS = 12; //max number of charms in preview area

(function(window, document, $, undefined) {

    app.createFunctions = app.createFunctions || {};
    app.createFunctions.reloadListeners = function() {

            $( "#tabs" ).css("visibility", "visible");
            
            // $( "#tabs" ).tabs({ 
            // // set the default tab.  0=connectors, etc.
            //  selected: 1,
            //  show: function(event, ui) {
            //      $(ui.panel).css('border-color', $(ui.tab).css('color') );
            //      $( "#tabs > ul" ).css('border-color', $(ui.tab).css('color') ) ;
            //  }
            // });

            $( ".charm" ).charm_popup();
            $( ".connector" ).connector_popup();
            
            $( ".charm" ).draggable({
                helper: "clone",
                appendTo: "body",
                handle: "img.handle",
                drag: function( event, ui ) {

                    ui.helper.find(".charm_hover_popup").css("display","none");

                    var drop_in_pos = 1;
                    $( "#inner_droppable > div.charm" ).each(function() {
                        if (ui.offset.left < $( this ).offset().left) {
                            return false;
                        }
                        drop_in_pos += 1;
                    });

                    $( this ).attr( "drop_in_pos", drop_in_pos );
                }
            });

            $( ".connector" ).draggable({
                helper: "clone",
                
                drag: function( event, ui ) {
                    ui.helper.find(".charm_hover_popup").css("display","none");
                }
                
            });

            $( "#droppable" ).droppable({
                activeClass: "ui-state-hover",
                hoverClass: "ui-state-active",
                drop: function( event, ui ) {
                            
                            var elem = '';
                    // dropped a connector
                    if (ui.draggable.hasClass('connector')) {
                        elem = ui.draggable;
                        var elem_id = elem.attr("id");
                        var elem_product_id = elem.attr("pid");
                        var elem_price = elem.attr("price");

                        $( "#connector_droppable" ).attr("pid", elem_product_id);
                        $( "#connector_droppable" ).attr("price", elem_price);

                        if (elem_id == "connector-0") {
                            $( "#connector_droppable" ).css("background-image", "none");
                        }
                        else {
                            $( "#connector_droppable" ).css("background-image", ui.draggable.css("background-image"));
                        }

                        recalc_total_price();
                        show_hide_links();
                        save_pids_to_cookie();
                    }

                    // dropped a charm
                    if (ui.draggable.hasClass('charm')) {
                        // determine which type of charms have been dropped: 'one from the tabs' or an 'existing one on the preview area that was just moved'
                        // then proceed to 'add' or 'move'
                        elem = null;
                        var parent_id = ui.draggable.parent().get(0).id;
            
                        if (parent_id == 'inner_droppable') { // an existing element on the preview area is moved
                            elem = ui.draggable;
                        } 
                        else { // a new element is added to the preview area
                            charms_counter++;
                            elem = ui.draggable.clone();
                            elem.attr( "id", elem.attr("id") + "_" + charms_counter );

                            set_elem_draggable(elem);
                            elem.css("display","none");
                        }

                        append_elem_to_droppable(elem);
                        
                        recalc_total_price();
                        show_hide_links();
                        save_pids_to_cookie();
                    }
                },
                out: function( event, ui ) {
                    // set an attribute "dropped_out" that will be used to determine if the element should be removed when the dragging stops   
                    var parent_id = ui.draggable.parent().get(0).id;
                    if (parent_id == 'inner_droppable') {
                        ui.draggable.attr( "dropped_out", 1 );
                    }
                },
                over: function( event, ui ) {
                    var parent_id = ui.draggable.parent().get(0).id;
                    if (parent_id == 'inner_droppable') {
                        ui.draggable.removeAttr( "dropped_out" );
                    }
                }
            });

            set_elem_draggable = function(elem) {
                elem.css("width","74px");
                
                //resize the image and div
                elem.find( "img" ).removeAttr("width").removeAttr("height").css("position","relative").css("left","-3px");  
                    
                //remove text below the charm
                elem.find( "div.charm-short-name" ).remove();
                elem.find( "div.charm-price" ).css('margin-top','-15px');
                elem.find( "div.charm-out-stock" ).remove();
                
                elem.draggable({
                    helper: "original",
                    drag: function( event, ui ) {
                        elem.css( "z-index",1 );

                        $( "#msg" ).html('drag ' + $(this).offset().top + " " + $(this).offset().left);

                        var drop_in_pos = 1;
                        $( "#inner_droppable > div.charm" ).each(function() {
                            if (ui.offset.left < $( this ).offset().left) {
                                return false;
                            }
                            drop_in_pos += 1;
                        });
                        elem.attr( "drop_in_pos", drop_in_pos );
                    },
                    stop: function( event, ui ) {
                        var parent_id = ui.helper.parent().get(0).id;
                        if (parent_id == 'inner_droppable') {
                            if (ui.helper.attr( "dropped_out" ) !== undefined) {
                                ui.helper.remove();
                                
                                recalc_total_price();
                                show_hide_links();
                                save_pids_to_cookie();

                                // when the number of elements in the preview area becomes lower than 10, enable the drag feature
                                var children = $("#inner_droppable").children( "div.charm" ); // get elements currently placed on the preview area
                                if (children.length <= MAX_NUM_CHARMS-1) {
                                    $( "#tabs .charm" ).draggable("enable");
                                }
                            }
                        }
                    }
                });
            };

            append_elem_to_droppable = function(elem) {
                elem.fadeOut(function() {
                    var inner_droppable = $( "#inner_droppable" );
                    var children = inner_droppable.children( "div.charm" ); // get elements currently placed on the preview area
                    var drop_in_pos = elem.attr( "drop_in_pos" );

                    // place the new element in the position specified in 'drop_in_pos'
                    if (children.length === 0) {
                        elem.appendTo( inner_droppable ).fadeIn();
                    } 
                    else if (drop_in_pos > children.length) {
                        elem.appendTo( inner_droppable ).fadeIn();
                    }
                    else {
                        elem_next = inner_droppable.children( "div.charm" )[drop_in_pos-1];
                        
                        if ( $( elem_next ).attr('id') != elem.attr('id') ) {
                            elem.insertBefore(elem_next).fadeIn();
                        } 
                        else {
                            elem.fadeIn();
                        }
                    }

                    elem.css( "top","auto" ).css( "left","auto" ).css( "z-index","auto" );
                });

                var inner_droppable = $( "#inner_droppable" );
                var children = inner_droppable.children( "div.charm" ); // get elements currently placed on the preview area

                // when the number of elements in the preview area will reach 10, disable the drag feature
                if (children.length >= MAX_NUM_CHARMS) {
                    $( "#tabs .charm" ).draggable("disable");
                }
            };

            recalc_total_price = function() {
                var total = 0;
                
                //connector price
                total = total + parseFloat($( "#connector_droppable" ).attr("price"));
                
                //components' price
                $( "#inner_droppable > div.charm" ).each(function() {
                    total = total + parseFloat($( this ).attr("price"));
                });
                total = total.toFixed(2);
                
                if (total > 0)
                {
                    $( "#creation_total_price" ).html('$'+total);
                }
                else
                {
                    $( "#creation_total_price" ).html('');
                }
            };
            
            load_images_droppable = function() {
                // load the charm images on the droppable area
                $( "#inner_droppable" ).find( "div.charm img" ).each(function() {
                    var img_src = $( this ).attr('img_src') ;
                    $( this ).attr('src', img_src);
                });
            };
    };
})(window, document, jQuery);

$(function() { 
    app.createFunctions.reloadListeners();
});
