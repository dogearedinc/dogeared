
$(function($) {

		/* Hide tabs random on page load */
	//$('#tabs-random').hide();

		// Step 2 button click
	$('#btn_step2 a').on('click', function(){
		step2_clicked(true);
		return false;
	});

		/* Step 3 button click */
	$('#btn_step3 a').on('click', function(e){

		e.preventDefault();

		submit_products_cart();

	});
		/* Add product to droppable area */
	$('.button_add_to_creation').on('click', function(){
		anim_add_to_droppable(this, $(this).attr("data-type"), $(this).attr("data-id"));
		return false;
	});

		/* Add product to cart */
	$('.button_add_to_cart').on("click", function(e){

		e.preventDefault();

		var postdata = "Quantity=1" + "&pid="+ $(this).attr("data-id");

		app.cart.update(postdata, function (response) {

			app.quickView.close();

			app.minicart.show(response);
		});

		//app.minicart.add( "", postdata);
		return false;
	});

	$('#buy_this_creation').on("click", function(e){

		e.preventDefault();

		submit_products_cart();

		//app.minicart.add( "", postdata);
		return false;
	});

	$('.button_add_inspiration_to_cart').on("click", function(e){

		e.preventDefault();

		var postdata = "&editAction=Add" + "&inspirationID="+ $(this).attr("data-id");

		var url = app.util.ajaxUrl(app.urls.nbAddInspiration);

		$.post(url + postdata, postdata, function(response){

			app.quickView.close();

			app.minicart.show(response);
		});

		//app.minicart.add( "", postdata);
		return false;
	});

});


function submit_products_cart(callback){

	var pids = get_creation_pids(true);
	if (pids === "") { return false; }

	pids = pids.split("|").join(",");

	var qty = [];

	//childIDs = childIDs.join();
	qty = qty.join();



	var postdata = "pids="+ pids;

	var url = app.util.ajaxUrl(app.urls.nbAddCreation);

	$.post(url, postdata, function(response){

		app.quickView.close();

		app.minicart.show(response);
	});

	if(callback){
		callback();
	}
	/*
	app.cart.update(postdata, function (response) {

		app.quickView.close();

		app.minicart.show(response);

	});
	*/
	//app.minicart.add( "", postdata);
}

function show_tab(tab_name) {

    //reset filter
	filter_charms_by_material('');
	filter_connectors_by_material('');

	$("#tabs").children().css("display", "none");

	if (tab_name !== "") {
		// load the charm images ... if they haven't been loaded yet
		$("#" + tab_name).find( "div.charm[data-displayed=1] img" ).each(function() {
			var img_src = $( this ).attr('img_src') ;
			$( this ).attr('src', img_src);
		});

		$("#" + tab_name).css("display", "block");
		$("#" + tab_name).find("h1").css("display", "block");

		if (tab_name == "tabs-random") {
			$("#btn_load_more").css("display","inline-block");
		} else {
			$("#btn_load_more").css("display","none");
		}
	}
}
(function(window, document, $, undefined) {

    app.createFunctions = app.createFunctions || {};

    app.createFunctions.updateCharms = function() {


        /* Add product to droppable area */

        $('.button_add_to_creation').on('click', function(){

            anim_add_to_droppable(this, $(this).attr("data-type"), $(this).attr("data-id"));

            return false;

        });

        /* Add product to cart */

        $('.button_add_to_cart').on("click", function(e){

            e.preventDefault();

            var postdata = "Quantity=1" + "&pid="+ $(this).attr("data-id");

            app.cart.update(postdata, function (response) {

            app.quickView.close();

            app.minicart.show(response);

        });

        //app.minicart.add( "", postdata);

        return false;

        });


        $('.button_add_inspiration_to_cart').on("click", function(e){

            e.preventDefault();

            var postdata = "&editAction=Add" + "&inspirationID="+ $(this).attr("data-id");

            var url = app.util.ajaxUrl(app.urls.nbAddInspiration);

            $.post(url + postdata, postdata, function(response){

                app.quickView.close();

                app.minicart.show(response);

            });

            //app.minicart.add( "", postdata);

            return false;

        });

    };
    app.createFunctions.updateCharms();

})(window, document, jQuery);

function show_all_charms() {

     //reset filter
	filter_charms_by_material('');
	filter_connectors_by_material('');

	$("#tabs").children().css("display", "block");
	$("#tabs").children().find("h1").css("display", "block");

	// the life-is-rosy is hidden when showing all charms
	$("#tabs-life-is-rosy").css("display", "none");
}

var filtered_by_material_type = ''; // global
function filter_charms_by_material(type) {

	filtered_by_material_type = type;

	$("#msg_no_charms").css("display","none");

	if (type !== "") {

		// if page is displaying the life-is-rosy charms
		if ($("#tabs-life-is-rosy").css("display") != "none") {
			// if tabs-random exists, it means we are on the 'create' page
			if ($("#tabs-random").length > 0) {
				show_tab("tabs-random");
			} else {
				show_all_charms();
			}
		}

		// make the charm divs of the selected material (gold, silver, etc) visible
		$("#tabs").find("div.charm[data-displayed=1]").css("display", "none");
		$("#tabs").find("div."+type+"[data-displayed=1]").css("display", "block");

		// hide the empty charm divs
		$("#tabs").find("div.charm_empty[data-displayed=1]").css("display", "none");

		// check if there are no charms for the selected tag
		var num_charms = $("#tabs").find("div.charm[data-displayed=1]:visible").length;
		if (num_charms === 0) {
			$("#msg_no_charms").css("display","block");
		}

		// hide headers of the sections
		$("#tabs").children().each(function() {
			var num_charms_section = $(this).find("div.charm[data-displayed=1]:visible").length;
			if (num_charms_section === 0) {
				$(this).find("h1").hide();
			} else {
				$(this).find("h1").show();
			}
		});

	} else {
		// make all charm divs visible
		$("#tabs").find("div.charm[data-displayed=1]").css("display", "block");

		// display the empty charm divs
		$("#tabs").find("div.charm_empty[data-displayed=1]").css("display", "block");
	}

}

function filter_connectors_by_material(type) {

	$("#msg_no_connectors").css("display","none");

	if (type !== "") {
		// make the connector divs of the selected material (gold, silver, etc) visible
		$("#tabs").find("div.connector").css("display", "none");
		$("#tabs").find("div."+type).css("display", "block");

		// check if there are no connectors for the selected tag
		var num_connectors = $("#tabs").find("div.connector:visible").length;
		if (num_connectors === 0) {
			$("#msg_no_connectors").css("display","block");
		}

	} else {
		// make all connector divs visible
		$("#tabs").find("div.connector").css("display", "block");
	}
}

function filter_by_length(length) {

	$("#msg_no_connectors").css("display","none");

	if (length !== "") {
		// make the connector divs of the selected length visible
		$("#tabs").find("div.connector").css("display", "none");
		$("#tabs").find("div."+length).css("display", "block");

		// check if there are no connectors for the selected length
		var num_connectors = $("#tabs").find("div.connector:visible").length;
		if (num_connectors === 0) {
			$("#msg_no_connectors").css("display","block");
		}
	} else {
		// make all connector divs visible
		$("#tabs").find("div.connector").css("display", "block");
	}
}

function step1_clicked(do_scroll) {

	$("#btn_step2").css("display","inline-block");
	$("#leftnav_chains").css("display","inline-block");

	$("#leftnav_charms").css("display","none");
	$("#btn_step3").css("display","none");
	$("#btn_load_more").css("display","none");

	$("#header_step1").removeClass("header_transparent");
	$("#header_step2").addClass("header_transparent");

	show_tab("tabs-connector");

	if (do_scroll) {
		window.scrollTo(0, 0);
	}
}

function step2_clicked(do_scroll) {

	$("#btn_step2").css("display","none");

	$("#btn_step3").css("display","inline-block");
	$("#btn_load_more").css("display","inline-block");

	$("#header_step1").addClass("header_transparent");
	$("#header_step2").removeClass("header_transparent");

	show_tab("tabs-random");

	if (do_scroll) {
		window.scrollTo(0, 0);
	}
}

function step3_clicked() {

	create_add_to_basket__creation(0);

	window.scrollTo(0, 0);
}

function load_more_clicked() {
	var tabs_random = $("#tabs-random");
	if (tabs_random.css("display")!="none") {
		var last_charm = tabs_random.find(".charm[data-displayed=1]").last();
		var group_number = parseInt(last_charm.attr("data-group"), 10);
		var next_group = parseInt((group_number + 1), 10);

		tabs_random.find(".charm[data-group="+next_group+"]").css("display","block").attr("data-displayed",1);
		tabs_random.find(".charm[data-group="+next_group+"] img").each(function() {
			var img_src = $( this ).attr('img_src') ;
			$( this ).attr('src', img_src);
		});

		if (filtered_by_material_type !== '') {
			filter_charms_by_material(filtered_by_material_type);
		}
	}
}

save_pids_to_cookie = function() {
	var pids = get_creation_pids(false);
	if (pids === "") { return false; }

	document.cookie = 'create_pids=' + escape(pids) + ';path=/';
};

// get product ids of the creation in the preview area
function get_creation_pids(display_alert) {
	var pids = "";

	// connector
	var connector_pid = $( "#connector_droppable" ).attr("pid");
	if (connector_pid != "0") {
		pids += connector_pid;
	}

	// components
	var charms = $( "#inner_droppable > div.charm" );
	if (charms.length === 0 && connector_pid === "0")
	{
		if (display_alert) {
			alert("You need to add at least one component to the preview area");
		}
		return "";
	}

	// product ids of the components
	charms.each(function() {
		if (pids !== "") { pids += "|"; }
		pids += $( this ).attr("pid");
	});

	return pids;
}

function remove_connector() {
	$( "#connector_droppable" ).attr("pid", "0").attr("price", "0").css("background-image", "none");
	recalc_total_price();
	show_hide_links();
	save_pids_to_cookie();
	return false;
}

function show_hide_links()
{
	var connector_droppable = $( "#connector_droppable" );
	var connector_droppable_pid = connector_droppable.attr("pid");

    if (connector_droppable_pid == "0")
	{
		$( "#link_remove_connector" ).css("display", "none");
	}
	else
	{
		$( "#link_remove_connector" ).css("display", "block");
	}

	var charms = $( "#inner_droppable > div.charm" );
	if (charms.lengthu === 0 && connector_droppable_pid == "0")
	{
		$( "#link_reset_creation" ).css("display", "none");
		$( ".hide_on_empty" ).css("visibility", "hidden");
	}
	else
	{
		$( "#link_reset_creation" ).css("display", "block");
		$( ".hide_on_empty" ).css("visibility", "visible");
	}
}


function create_add_to_basket__product(o, type, pid) {
	var div_elem=document.getElementById('addcart1_popin_div');
	var con_elem=document.getElementById('addcart1_popin_content');

	//type is 'charm' or 'connector'
	$("#tabs").find("."+type+"[pid="+pid+"]").find(".charm_hover_popup").css("display","none");


	var ajaxRequest=it_ajax_request();
	if (!ajaxRequest) return false;

	div_elem.style.visibility = 'visible';

	div_top = parseInt($("#tabs").find("div[pid="+pid+"]").offset().top, 10);
	div_elem.style.top = (div_top - 120) + 'px';

	con_elem.innerHTML="&lt;span class='makeitblack'&gt;adding product...&lt;/span&gt;";

	if(parseFloat(pid) != parseInt(pid, 10) || parseInt(pid, 10)<=0) {
		con_elem.innerHTML="&lt;span class='makeitred'&gt;Product Not Found&lt;/span&gt;";
		return false;
	}

	ajaxRequest.onreadystatechange = function() {
		if(ajaxRequest.readyState == 4)
		{
			con_elem.innerHTML=ajaxRequest.responseText;
		}
	};

	var theurl='http://www.dogeared.com/add_product_ajax.php';

	theurl=theurl+"?product_id=" + pid + '&amp;quantity=1';
	ajaxRequest.open("GET", theurl, true);
	ajaxRequest.send(null);

	return false;
}


function create_add_to_basket__creation(buy_available) {
	var pids = get_creation_pids(true);
	if (pids === "") { return false; }

	var div_elem=document.getElementById('addcart_popin_div');
	var con_elem=document.getElementById('addcart_popin_content');

	var ajaxRequest=it_ajax_request();
	if (!ajaxRequest) return false;


	div_elem.style.visibility = 'visible';
	con_elem.innerHTML="&lt;span class='makeitblack'&gt;adding products...&lt;/span&gt;";

	ajaxRequest.onreadystatechange = function() {
		if(ajaxRequest.readyState == 4) {
			con_elem.innerHTML=ajaxRequest.responseText;
		}
	};

	// Check stock of products ... if any of them doesn't have enough stock the popup should allow users to buy available items, and add to waiting list the others.
	// If all products are available, add them to the basket.
	var theurl='http://www.dogeared.com/design-your-own/add_product_multiple_ajax.php';

	theurl=theurl+"?product_ids=" + pids + '&amp;parent_build=0&amp;buy_available=' + buy_available;
	ajaxRequest.open("GET", theurl, true);
	ajaxRequest.send(null);

	window.scrollTo(0, 0);

	return false;
}

function create_add_to_droppable(o, type, pid) {
	if (type == 'charm') {

		if ($("#inner_droppable").children( "div.charm" ).length >= MAX_NUM_CHARMS) {
			alert('You can only add up to ' + MAX_NUM_CHARMS + ' charms to your creation');
			return;
		}

		var charm = $("#tabs").find("."+type+"[pid="+pid+"]");

		charms_counter++;
		var new_charm = charm.clone().first(); // some charms appear in more than one category (because they have several 'create' tags)
		new_charm.find(".charm_hover_popup").css("display","none");
		new_charm.attr("id", new_charm.attr("id") + "_" + charms_counter);
		new_charm.css("display","block").attr("data-displayed","1");

		// another div with the same id, can have its images not updated
		new_charm.find( "img" ).each(function() {
			var img_src = $( this ).attr('img_src') ;
			$( this ).attr('src', img_src);
		});


		$("#inner_droppable").append(new_charm);

		if ($("#inner_droppable").children( "div.charm" ).length >= MAX_NUM_CHARMS) {
			$( "#tabs .charm" ).draggable("disable");
		}

		set_elem_draggable(new_charm);

		recalc_total_price();
		show_hide_links();
		save_pids_to_cookie();
	}
	else if (type == 'connector') {

		var elem = $("#tabs").find("."+type+"[pid="+pid+"]");

		var elem_id = elem.attr("id");
		var elem_product_id = elem.attr("pid");
		var elem_price = elem.attr("price");

		$( "#connector_droppable" ).attr("pid", elem_product_id);
		$( "#connector_droppable" ).attr("price", elem_price);

		$( "#connector_droppable" ).css("background-image", elem.css("background-image"));

		recalc_total_price();
		show_hide_links();
		save_pids_to_cookie();
	}
}

function anim_add_to_droppable(o, type, pid)  {
	if ($("#inner_droppable").children( "div.charm" ).length >= MAX_NUM_CHARMS) {
		alert('You can only add up to ' + MAX_NUM_CHARMS + ' charms to your creation');
		return;
	}

	var droppable_offset = $('#droppable').offset(),
            offset = '';

	if (type == 'charm') {

		var charm = $(o).parents(".charm").first();

		var charm1_1 = charm.clone();
		offset = charm.offset();

		charm1_1.appendTo(document.body);
		charm1_1.css("position","absolute").css("display","block");
		charm1_1.find( "img" ).each(function() {
			var img_src = $( this ).attr('img_src') ;
			$( this ).attr('src', img_src);
		});
		charm1_1.find(".charm_hover_popup").hide();

		charm1_1.tween({
			left:{ start: offset.left, stop: (droppable_offset.left+340), time: 0, units: 'px', duration: 1, effect:'circOut' },
			top:{ start: offset.top, stop: (droppable_offset.top+20), time: 0, units: 'px', duration: 1, effect:'easeInOut' },
			onStop: function( element ){
				charm1_1.remove();

				create_add_to_droppable(o, type, pid);
			}
		});

		$.play();
	}
	else if (type == 'connector') {

		var elem = $("#tabs").find("."+type+"[pid="+pid+"]");

		var connector = elem.clone();
		offset = elem.offset();
		connector.appendTo(document.body);
		connector.css("position","absolute");
		connector.find(".charm_hover_popup").hide();

		connector.tween({
			left:{ start: offset.left, stop: (droppable_offset.left+10), time: 0, units: 'px', duration: 1, effect:'easeInOut' },
			top:{ start: offset.top, stop: (droppable_offset.top+10), time: 0, units: 'px', duration: 1, effect:'easeInOut' },
			onStop: function( element ){
				connector.remove();

				create_add_to_droppable(o, type, pid);
		}
		});

		$.play();
	}
}

function create_add_to_wishlist() {
	var pids = get_creation_pids(true);
	if (pids === "") { return false; }

	it_popin_open('http://www.dogeared.com/design-your-own/wishlist-ajax.php?pids=' + pids + '&amp;parent_build=0&amp;method=frame', 'wishlist_popin_div', 'wish_popin_content');
}

function create_drop_a_hint() {
	var pids = get_creation_pids(true);
	if (pids === "") { return false; }

	it_popin_open('http://www.dogeared.com/drop-a-hint.php?pids=' + pids + '&amp;parent_build=0&amp;method=frame&amp;popin=1', 'drophint_popin_div', 'drophint_popin_content');
}

function create_tell_a_friend() {
	var pids = get_creation_pids(true);
	if (pids === "") { return false; }

	it_popin_open('http://www.dogeared.com/tell-a-friend.php?pids=' + pids + '&amp;parent_build=0&amp;method=frame&amp;popin=1', 'tellfriend_popin_div', 'tellfriend_popin_content');
}

function create_share() {
	var pids = get_creation_pids(false);

	it_popin_open('http://www.dogeared.com/design-your-own/share.php?pids=' + pids + '&amp;parent_build=0&amp;method=frame&amp;popin=1', 'share_popin_div', 'share_popin_content');
}

function create_join_waiting_list(pids) {
	if (pids === "") { return false; }

	it_popin_open('http://www.dogeared.com/waitinging-list.php?pids=' + pids + '&amp;method=frame&amp;popin=1', 'waitlist_popin_div', 'waitlist_popin_content');
}

function create_login_to_save() {
	var pids = get_creation_pids(true);
	if (pids === "") { return false; }

	document.cookie = 'create_pids=' + escape(pids) + ';path=/';

	document.location = 'https://www.dogeared.com/secure/account/index.php?goto_url=http%3A%2F%2Fwww.dogeared.com%2Fdesign-your-own%2Fcreate';
}

function reset_creation() {
		document.location = "http://www.dogeared.com/design-your-own/create?reset=1";
		return false;
}
