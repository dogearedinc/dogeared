﻿
$(window).load(function () {
	
	var shipWidth = "90%",
	shipHeight = "90%";
	
	function testWidth(){
		if( $(document).width() > 749 ){
			shipWidth = "60%";
			shipHeight = "35%";		
			
		}else{
			shipWidth = "90%";
			shipHeight = "90%";
			
		}
	}
	
	function doOnOrientationChange()
	{
		testWidth();
		$.colorbox.resize({
			width: shipWidth,
	        height: shipHeight,
		})
		//window.scrollTo( 0, $('#cboxWrapper').top ); 
	}
	
	function addModals(){
		$(".iframe").colorbox({
	        rel: 'iframe',
	        iframe: true,
	        width: "90%",
	        height: "80%",
	        transition: 'fade',
	        speed: 500,
	        maxWidth: '1000px',
	        opacity:.4,
	        title:false,
	        close:"x",
	        next:">",
	        previous:"<"
	    });
		
		$(".bridalframe").colorbox({
	        rel: 'iframe',
	        iframe: true,
	        width: "90%",
	        height: "80%",
	        transition: 'fade',
	        speed: 500,
	        maxWidth: '1000px',
	        opacity:.4,
	        title:false,
	        close:"x",
	        next:">",
	        previous:"<",
	        className: 'bridalcbox'
	    });
	    
	    $(".sizing").colorbox({
	        rel: 'sizing',
	        iframe: true,
	        width: shipWidth,
	        height: shipHeight,
	        transition: 'fade',
	        opacity:.4,
	        speed: 500,
	        title:false,
	        close:"x",
	        previous:"",
	        next:""
	    });
	}
	
	window.addEventListener('orientationchange', doOnOrientationChange);
	testWidth();
	addModals();
	
	//scroll arrow with header
	$(document).bind('cbox_complete', function(){
		var modalIframe = window.document.getElementsByClassName('cboxIframe')[0];
		if(modalIframe){
			var cboxNext = window.document.getElementById('cboxNext');
			var cboxPrev = window.document.getElementById('cboxPrevious');
			var cboxNextInitialTop = parseInt(window.getComputedStyle(cboxNext).getPropertyValue('top'),10);
			modalIframe.onload = function(){
				var iframeWindow = modalIframe.contentWindow;
				var initialScroll = iframeWindow.scrollY;
				iframeWindow.document.body.addEventListener('touchmove', function(){
					var scrollDifference = initialScroll - iframeWindow.document.getElementById('wrapper').scrollTop;
					//for iPhone
					if(scrollDifference < -16){
						$(cboxNext).fadeOut();
						$(cboxPrev).fadeOut();
					}else if(scrollDifference==0){
						$(cboxNext).fadeIn();
						$(cboxPrev).fadeIn();
					}
				});
				iframeWindow.document.addEventListener('scroll', function(){
					var scrollDifference = initialScroll - iframeWindow.document.body.scrollTop;
					cboxNext.style.top = (cboxNextInitialTop + scrollDifference) + "px";
					cboxPrev.style.top = (cboxNextInitialTop + scrollDifference) + "px";
				});
			} 
		}
	});
	
    
});


