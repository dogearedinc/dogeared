﻿$(document).ready(function () {
    // hide all of the divs
    $('.accordion:eq(0)> div.merch').hide();

    // apply onClick event handler to h3    
    $('.accordion:eq(0)> div.trigger').click(function () {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('active');
    });

    $(".contest").colorbox({
        rel: 'iframe',
        iframe: true,
        width: "60%",
        height: "55%",
        transition: 'fade',
        speed: 500
    });
});