﻿$(function () {
    $("#slides").slidesjs({
        navigation: {
            active: false,
            effect: "slide"
            // [string] Can be either "slide" or "fade".
        },
        pagination: {
            active: false
        },
        play: {
            effect: "slide",
            interval: 5000,
            auto: true,
            pauseOnHover: true,
            restartDelay: 2500
        }
    });
});

// ACCORDION
$(document).ready(function () {
    // hide all of the divs
    $('.accordion:eq(0)> div').hide();

    // apply onClick event handler to h3    
    $('.accordion:eq(0)> h3').click(function () {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('active');
    });

    // simulate click of first
    var elem = document.getElementById("open")
    elem.click();

});