﻿$(document).ready(function () {
    $('#naviToggle').click(function () {
        if (
                $('#naviMenu').is(':visible')
            ) {
            $('#naviMenu').hide();
            $('#shiftToggle').css('position', 'fixed');
            $('#shiftToggle').css('float', 'left');
            $('#content').css('float', 'left');
            $('#content').css('left', '0');
            $('#content').css('position', 'relative');
        }
        else {
            $('#naviMenu').show();
            $('#shiftToggle').css('position', 'relative');
            $('#shiftToggle').css('float', 'right');
            $('#content').css('left', '220px');
            $('#content').css('position', 'absolute');
        };
    });
});
