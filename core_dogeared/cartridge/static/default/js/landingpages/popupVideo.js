﻿$(document).ready(function () {
    $(".iframe1").colorbox({
        rel: 'iframe',
        iframe: true,
        width: "90%",
        height: "80%",
        transition: 'none',
        speed: 0,
        rel: "group1",
        preloading: false,
        arrowKey: false,
        maxHeight: 310,
        maxWidth: 650,
        closeButton: false
    });
    $(".iframe2").colorbox({
        rel: 'iframe',
        iframe: true,
        width: "80%",
        height: "80%",
        transition: 'none',
        speed: 0,
        rel: "group2",
        preloading: false,
        arrowKey: false,
        maxHeight: 300,
        maxWidth: 650
    });
    $(".iframe3").colorbox({
        rel: 'iframe',
        iframe: true,
        width: "80%",
        height: "80%",
        transition: 'none',
        speed: 0,
        rel: "group3",
        preloading: false,
        arrowKey: false,
        maxHeight: 300,
        maxWidth: 650
    });
});