/**
* Gift Card Inquiry Script
* This script creates an Inquiry SOAP request
*
*	Input and Output Parameters:
**	Form Input Parameters:
*    @input amount : String
*    @input fnameTo : String
*    @input lnameTo : String
*    @input recipientEmail : String
*	 @output giftCardId : String
*	 @output giftCardPin : String
*    @output transactionId : String
*/
importPackage( dw.system );
importClass(dw.net.HTTPClient);
importScript("int_profitpoint:Configuration.ds");
importScript("int_profitpoint:Util.ds");

function execute( pdict : PipelineDictionary ) : Number
{
	// Allocate a new card
	var json = cardAllocationJson(giftCardSet());
	var allocateResponse : HTTPClient = performApiRequest("allocate", json);
	
	var response = JSON.parse(allocateResponse.text);
	
	if(allocateResponse.statusCode != 200 || !response["success"]) {
		Logger.error("GiftCardAllocation, failed to allocate card, received code {0}, response {1}", 
			allocateResponse.statusCode, allocateResponse.text);
		return PIPELET_ERROR;
	}
	
	pdict.giftCardId = response.cardNumber;
	pdict.giftCardPin = response.pin;
	pdict.transactionId = response.requestRef;
	
	//Information about the reciever of the gift card
	var fname_to : Object = pdict.fnameTo;
	var lname_to : Object = pdict.lnameTo;
	var recipientEmail : Object = pdict.recipientEmail;
	
	// Send another request to update the card holder information
	var updateCustomerJson = JSON.stringify({"cardNumber": pdict.giftCardId, "pin": pdict.giftCardPin, "primaryCustomer":{"firstName":fname_to, "lastName": lname_to, "email": recipientEmail}});
	var updateResponse : HTTPClient = performApiRequest("updateAccount", updateCustomerJson);
	if(updateResponse.statusCode != 200) {
		Logger.error("GiftCardAllocation, failed to update customer information, received code {0}, response {1}", 
			updateResponse.statusCode, updateResponse.errorText);
	}
	 
    return PIPELET_NEXT;
}

