/**
* loyalty Card Inquiry Script
* This script creates an Inquiry SOAP request
*
*	Input and Output Parameters:
*
*    @input loyaltyCertificateID : String The loyalty Certificate ID.
*    @input loyaltyCertificatePin : String The loyalty Certificate Pin.
*	 @output balances : Object
*    @output loyaltyCertificateID : String the loyalty certificate ID
*	 @output validCard : Boolean 
*	 @output errorCode : String
*/
importPackage( dw.system );
importPackage( dw.order );
importPackage( dw.value );
importPackage( dw.util );
importPackage( dw.object);
importScript("int_profitpoint:Configuration.ds");
importScript("int_profitpoint:Util.ds");


function execute( pdict : PipelineDictionary ) : Number
{
	//loyalty Card Number. This value is received from the form.
	var loyaltyCertID : Object = pdict.loyaltyCertificateID;
	var loyaltyCertPin : Object = pdict.loyaltyCertificatePin;
	pdict.balances = new Object();
	pdict.validCard = false;
	
	var postData = cardBalanceLookupJson(loyaltyCertID, loyaltyCertPin, LOYALTY_CARD());
	Logger.info("LoyaltyCardScriptInquiry: checking balance");
	var rawResponse : dw.net.HTTPClient = performApiRequest("search", postData);
	
	// Check if we received a status 200 response code
	if(rawResponse.statusCode != 200) {
		Logger.error("LoyaltyCardScriptInquiry, received {0}, response: {1}", rawResponse.statusCode, rawResponse.errorText); 
		pdict.validCard = false;
		pdict.errorCode = JSON.parse(rawResponse.errorText).errorCode.toString();
		return PIPELET_ERROR;
	}
	
	var responseJson = JSON.parse(rawResponse.text);
	var card = responseJson["cards"][0];
	
	// Make sure the card was found
	if (responseJson["cards"].length == 0) {
		Logger.debug("LoyaltyCardScriptInquiry, could not find card {0}", loyaltyCertID);
		pdict.validCard = false;
		pdict.errorCode = responseJson.errorCode.toString();
		return PIPELET_ERROR;
	}
	
	pdict.validCard = true;
	pdict.errorCode = "";
	
	// Return all balances, using either the type of balance or balance code, if it is available
	card["balances"].forEach(function(balance) {
		// Ignore reserved balances
		if(balance["isHold"]) {
			return;
		}
		
		if(balance["balanceType"] == "Currency" || balance["balanceType"] == "Custom") {
			setOrIncrement(pdict.balances, balance["balanceCode"], balance["amount"]);
		} else {
			setOrIncrement(pdict.balances, balance["balanceType"], balance["amount"]);
		}
	});
	
    return PIPELET_NEXT;
}

/**
* Sets the balance that belongs to balanceName to amount or, if the value already exists, increments with amount.
*/
function setOrIncrement(balances : Object, balanceName: String, amount: Number) {
	if(balances[balanceName] > 0) {
		balances[balanceName] += amount;
	} else {
		balances[balanceName] = amount;
	}
}